-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22-Nov-2019 às 01:37
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parceriaafsist`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) CHARACTER SET utf8 NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  `uf` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `situacao` tinyint(1) DEFAULT NULL,
  `id_vendedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `nome`, `telefone`, `celular`, `cidade`, `uf`, `email`, `situacao`, `id_vendedor`) VALUES
(1, 'regivaldo', '(43) 9175-9496', NULL, 'Londrina', 'Paraná', 'oscardosopereira008@gmail.com', 1, 33),
(2, 'antonio carlos', '(43) 3223-2278', NULL, 'Cambé', 'Paraná', 'xavantesmetais@hotmail.com', 1, 33),
(3, 'paulo', '(43) 3361-5642', NULL, 'Londrina', 'Paraná', 'paulinhodandrea@gmail.com', 1, 33),
(4, 'jair', '(46) 9941-5063', NULL, 'Quedas do Iguaçu', 'Paraná', 'jairstachelski@hotmail.com', 1, 33),
(5, 'luisane', '(53) 9906-1068', NULL, 'Bagé', 'Rio Grande do Sul', 'luhernandes88@hotmail.com', 1, 33),
(6, 'edinho', '(55) 9923-3335', NULL, 'Vicente Dutra', 'Rio Grande do Sul', 'graciantunes@hotmail.com', 1, 33),
(7, 'MARCELO', '(54) 9617-1902', NULL, 'Nova Araçá', 'Rio Grande do Sul', 'MARCELO-V12@HOTMAIL.COM', 1, 34),
(8, 'carolina', '(53) 9993-7924', NULL, 'Rio Grande', 'Rio Grande do Sul', 'carol.garcia197@gmail.com', 1, 33),
(9, 'elcio', '(48) 9615-2492', NULL, 'São José', 'Santa Catarina', 'alcioosnicamacho3@gmail.com', 1, 33),
(10, 'taiane', '(53) 9945-6975', NULL, 'Rio Grande', 'Rio Grande do Sul', 'cristianofinanceiro38@outlook.com', 1, 33),
(12, 'Maria', '(54) 9999-9999', '', 'Água Santa', 'Rio Grande do Sul', 'mariadastintas@yahoo.com.br', 1, 45),
(13, 'Joao', '(54) 9999-9999', '', 'Bonfim', 'Roraima', 'joaodoscaminhao@hotmail.com', 1, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `historico`
--

CREATE TABLE `historico` (
  `id` int(11) NOT NULL,
  `data` date NOT NULL,
  `id_vendedor` int(11) NOT NULL,
  `id_proposta` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `valor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `historico`
--

INSERT INTO `historico` (`id`, `data`, `id_vendedor`, `id_proposta`, `id_cliente`, `valor`) VALUES
(1, '2019-11-21', 2, 1, 12, '400.000,00'),
(2, '2019-11-21', 2, 1, 12, '1.000.000,00'),
(3, '2019-11-21', 2, 1, 12, '2.000.000,00'),
(4, '2019-11-21', 2, 1, 12, '500.000,00'),
(5, '2019-11-21', 3, 5, 13, '800.000,00'),
(6, '2019-11-21', 3, 5, 13, '2.000.000,00'),
(7, '2019-11-21', 3, 5, 13, '3.500.000,00'),
(8, '2019-11-21', 3, 5, 13, '150.000,00'),
(9, '2019-11-21', 3, 5, 13, '600.000,00'),
(10, '2019-11-21', 3, 5, 13, '1.000.000,00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `lembretes`
--

CREATE TABLE `lembretes` (
  `id` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL,
  `data` date NOT NULL,
  `texto` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `lembretes`
--

INSERT INTO `lembretes` (`id`, `id_vendedor`, `data`, `texto`) VALUES
(1, 22, '2017-12-04', ' MOACIR/ FLORES DA CUNHA\r\nPROPOSTA DE 2 MI\r\nTEL: 54 32928650'),
(3, 22, '2017-12-04', ' CLIENTE DONIZETE\r\nCAMPO MOURAO\r\n44 3810.1818\r\nIGOR LIGOU EM OUTUBRO, RETORNAR'),
(4, 22, '2017-12-04', ' OTAVIO MARINGÁ\r\n44 3033.3333\r\nIGOR LIGOU EM OUT. RETORNAR'),
(6, 22, '2018-01-22', ' LEONARDO ITAJAI\r\n47 30451439\r\nLIGAR VER SE PRECISA DE ALGO'),
(7, 22, '2018-01-22', ' JUAREZ CURITIBANOS\r\n49 32450382\r\nPEDIU PRA RETORNAR EM JAN'),
(8, 22, '2017-12-04', 'MARCELO MARINGA\r\n44 3028 2331 CONST. JUST\r\nPEDIU PRA RETORNAR AGORA'),
(42, 47, '2018-09-05', ' Matheus\r\nGran Para\r\nfone:(49) 3533 1560\r\ncredito: 350 mil.'),
(55, 38, '2019-01-10', ' LIGAR PARA JULIO MAKI MAQUINAS'),
(58, 33, '2019-12-10', ' Osmar Florianópolis\r\n48-99183-5686 waths\r\n'),
(63, 43, '2019-09-03', ' daniel -santiago 55984549393\r\ndc.barros@terra.com.br'),
(65, 43, '2019-10-03', ' claudia de cacador \r\n400.000 cef\r\nta sistema fone \r\n49988154145');

-- --------------------------------------------------------

--
-- Estrutura da tabela `propostas`
--

CREATE TABLE `propostas` (
  `id` int(11) NOT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `titulo` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `sequencia` int(10) DEFAULT NULL,
  `administradora` varchar(30) DEFAULT NULL,
  `valor` varchar(50) DEFAULT NULL,
  `linha1` varchar(100) DEFAULT NULL,
  `linha2` varchar(100) DEFAULT NULL,
  `linha3` varchar(100) DEFAULT NULL,
  `linha4` varchar(100) DEFAULT NULL,
  `linha5` varchar(100) DEFAULT NULL,
  `linha6` varchar(100) DEFAULT NULL,
  `linha7` varchar(250) NOT NULL,
  `tipo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `propostas`
--

INSERT INTO `propostas` (`id`, `descricao`, `titulo`, `sequencia`, `administradora`, `valor`, `linha1`, `linha2`, `linha3`, `linha4`, `linha5`, `linha6`, `linha7`, `tipo`) VALUES
(157, 'VALOR  3.30% 1000', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 6, 'VALOR', '1.000.000,00', 'Prazo de pagamento:  200 Meses', 'Entrada: R$ 39.235,00', 'Valor das demais parcelas: R$ 6.235,00', 'Lance que vai ser descontado do crédito: R$ 350.000,00', 'Crédito que sobra descontado o lance: R$ 650.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 4.476,35', ' ', 1),
(158, 'VALOR  3.30% 1500', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 7, 'VALOR', '1.500.000,00', 'Prazo de pagamento:  200 Meses', 'Entrada: R$ 58.852,50', 'Valor das demais parcelas: R$ 9.352,50', 'Lance que vai ser descontado do crédito: R$ 525.000,00', 'Crédito que sobra descontado o lance: R$ 975.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 6.714,31', ' ', 1),
(159, 'VALOR  3.30% 2000', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 7, 'VALOR', '2.000.000,00', 'Prazo de pagamento:  200 Meses', 'Entrada: R$ 78.470,00', 'Valor das demais parcelas: R$ 12.470,00', 'Lance que vai ser descontado do crédito: R$ 700.000,00', 'Crédito que sobra descontado o lance: R$ 1.300.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 8.952,71', ' ', 1),
(160, 'VALOR  3.30% 2500', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 8, 'VALOR', '2.500.000,00', 'Prazo de pagamento:  200 Meses', 'Entrada: R$ 98.087,50', 'Valor das demais parcelas: R$ 15.587,50', 'Lance que vai ser descontado do crédito: R$ 875.000,00', 'Crédito que sobra descontado o lance: R$ 1.625.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 11.190,89', ' ', 1),
(161, 'VALOR  3.30% 3000', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 9, 'VALOR', '3.000.000,00', 'Prazo de pagamento:  200 Meses', 'Entrada: R$ 117.705,00', 'Valor das demais parcelas: R$ 18.705,00', 'Lance que vai ser descontado do crédito: R$ 1.050.000,00', 'Crédito que sobra descontado o lance: R$ 1.950.00,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 13.428,62', ' ', 1),
(162, 'VALOR  3.30% 3500', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 10, 'VALOR', '3.500.000,00', 'Prazo de pagamento:  200 Meses', 'Entrada: R$ 137.322,50', 'Valor das demais parcelas: R$ 21.822,50', 'Lance que vai ser descontado do crédito: R$ 1.225.000,00', 'Crédito que sobra descontado o lance: R$ 2.275.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 15.667,24', ' ', 1),
(164, 'VALOR  3.30% 4000', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 11, 'VALOR', '4.000.000,00', 'Prazo de pagamento:  200 Meses', 'Entrada: R$ 156.940,00', 'Valor das demais parcelas: R$ 24.940,00', 'Lance que vai ser descontado do crédito: R$ 1.400.000,00', 'Crédito que sobra descontado o lance: R$ 2.600.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 17.905,42', ' ', 1),
(182, '*Financiamento sobra 100', 'TAXA DE 1,35% AO MÊS.', 101, NULL, '100.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'VALOR DAS PARCELAS: R$ 2.217,00', 'SALDO DEVEDOR: R$ 443.400,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(183, '*Financiamento sobra 150', 'TAXA DE 1,35% AO MÊS.', 102, NULL, '150.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'VALOR DAS PARCELAS: R$ 3.033,00', 'SALDO DEVEDOR: R$ 606.600,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(184, '*Financiamento sobra 200', 'TAXA DE 1,35% AO MÊS.', 103, NULL, '200.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'VALOR DAS PARCELAS: R$ 3.806,00', 'SALDO DEVEDOR: R$ 761.200,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(185, '*Financiamento sobra 300', 'TAXA DE 1,35% AO MÊS.', 104, NULL, '300.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'VALOR DAS PARCELAS: R$ 4.935,00', 'SALDO DEVEDOR: R$ 987.000,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(186, '*Financiamento sobra 400', 'TAXA DE 1,35% AO MÊS.', 105, NULL, '400.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'VALOR DAS PARCELAS: R$ 6.521,00', 'SALDO DEVEDOR: R$ 1.304.200,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(187, '*Financiamento sobra 500', 'TAXA DE 1,35% AO MÊS.', 106, NULL, '500.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'VALOR DAS PARCELAS: R$ 8.069,00', 'SALDO DEVEDOR: R$ 1.613.800,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(188, '*Financiamento sobra 1,0', 'TAXA DE 1,35% AO MÊS.', 108, NULL, '1.000.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'VALOR DAS PARCELAS: R$ 16.369,00', 'SALDO DEVEDOR: R$ 3.273.800,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(189, '*Financiamento sobra 1,5', 'TAXA DE 1,35% AO MÊS.', 110, NULL, '1.500.000,00', 'Prazo de pagamento: 200 meses', 'VALOR DAS PARCELAS: R$ 24.170,00', 'SALDO DEVEDOR: R$ 4.834.000,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(192, '*Financiamento sobra 2,0', 'TAXA DE 1,35% AO MÊS.', 111, NULL, '2.000.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'VALOR DAS PARCELAS: R$ 31.909,00', 'SALDO DEVEDOR: R$ 6.381.800,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(209, 'CAIXA 150', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 80, 'CAIXA', '150.000,00', 'Prazo de pagamento: 197 Meses', 'ENTRADA: 951,78', 'Lance que vai ser descontado do crédito: R$ 75.000,00', 'Crédito que sobra descontado o lance: R$ 75.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 569,13', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(219, 'CAIXA 900', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 85, 'CAIXA', '900.000,00', 'Prazo de pagamento: 197 Meses', 'ENTRADA: R$ 5.710,66', 'Lance que vai ser descontado do crédito: R$ 450.000,00', 'Crédito que sobra descontado o lance: R$ 450.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 3.414,74', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(220, 'CAIXA 300', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 81, 'CAIXA', '300.000,00', 'Prazo de pagamento: 197 Meses', 'ENTRADA: R$ 1.903,55', 'Lance que vai ser descontado do crédito: R$ 150.000,00', 'Crédito que sobra descontado o lance: R$ 150.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 1.138,25', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(221, 'CAIXA 400', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 82, 'CAIXA', '400.000,00', 'Prazo de pagamento: 197 Meses', 'ENTRADA: R$ 2.538,07', 'Lance que vai ser descontado do crédito: R$ 200.000,00', 'Crédito que sobra descontado o lance: R$ 200.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 1.517,66', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(225, 'CAIXA 600', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 83, 'CAIXA', '600.000,00', 'Prazo de pagamento: 197 Meses', 'ENTRADA: R$ 3.807,10', 'Lance que vai ser descontado do crédito: R$ 300.000,00', 'Crédito que sobra descontado o lance: R$ 300.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 2.276,50', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(227, 'CAIXA 1200', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 86, 'CAIXA', '1.200.000,00', 'Prazo de pagamento: 197 Meses', 'ENTRADA: R$ 7.614,21', 'Lance que vai ser descontado do crédito: R$ 600.000,00', 'Crédito que sobra descontado o lance: R$ 600.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 4.552,98', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(228, 'CAIXA 1500', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 87, 'CAIXA', '1.500.000,00', 'Prazo de pagamento: 197 Meses', 'ENTRADA: R$ 9.517,75', 'Lance que vai ser descontado do crédito: R$ 750.000,00', 'Crédito que sobra descontado o lance: R$ 750.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 5.691,25', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(229, 'CAIXA 1800', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 88, 'CAIXA', '1.800.000,00', 'Prazo de pagamento: 197 Meses', 'ENTRADA: R$ 11.421,30', 'Lance que vai ser descontado do crédito: R$ 900.000,00', 'Crédito que sobra descontado o lance: R$ 900.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 6.829,50', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(232, 'CAIXA 200', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 80, 'CAIXA', '200.000,00', 'Prazo de pagamento: 197 Meses', 'ENTRADA: R$ 1.270,37', 'Lance que vai ser descontado do crédito: R$ 100.000,00', 'Crédito que sobra descontado o lance: R$ 100.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 758,83', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(233, '*Financiamento sobra 750', 'TAXA DE 1,35% AO MÊS ', 107, NULL, '750.000,00', 'Prazo de pagamento: 200 meses', 'VALOR DAS PARCELAS: R$ 12.499,00', 'SALDO DEVEDOR: R$ 2.499.800,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(235, '*Financiamento sobra 600', 'TAXA DE 1,35% AO MÊS ', 106, NULL, '600.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'VALOR DAS PARCELAS: R$ 10.116,00', 'SALDO DEVEDOR: R$ 2.023.200,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(243, 'CAIXA 2000', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 99, 'CAIXA', '2.000.000,00', 'Prazo de pagamento: 197 meses', 'Valor das parcelas: R$ 12.690,35', 'Lance que vai ser descontado do crédito: R$ 1.000.000,00', 'Crédito que sobra descontado o lance: R$ 1.000.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 7.588,30', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(247, 'CAIXA 700', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 84, 'CAIXA', '700.000,00', 'Prazo de pagamento: 197 meses', 'ENTRADA: R$ 4.441,62', 'Lance que vai ser descontado do crédito: R$ 350.000,00', 'Crédito que sobra descontado o lance: R$ 350.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 2.655,91', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(250, '*Financiamento sobra 4,0', 'TAXA DE 1,35% AO MÊS ', 119, NULL, '4.000.000,00', 'PRAZO DE PAGAMENTO: 200', 'VALOR DAS PARCELAS: R$ 63.425,00', 'SALDO DEVEDOR: R$ 12.685.000,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(251, '*Financiamento sobra 2,6', 'TAXA DE 1,10% AO MÊS ', 112, NULL, '2.600.000,00', 'Prazo de pagamento: 200 meses', 'VALOR DAS PARCELAS: R$ 36.316,84', 'SALDO DEVEDOR: R$ 7.263.368,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(261, '*Financiamento sobra 3,0', 'TAXA DE 1,35% AO MÊS ', 113, NULL, '3.000.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'VALOR DAS PARCELAS: R$ 47.947,00', 'SALDO DEVEDOR: R$ 9.589.400,00', 'Lance que vai ser descontado do crédito: R$ ', 'Crédito que sobra descontado o lance: R$ ', 'Saldo após contemplação por lance: R$ ', ' ', 3),
(262, 'PESADO 480', 'Taxa de 0,18% ao mês, 2,14% ao ano / Recolocação grupo já em andamento', 140, 'VALOR', '480.000,00', 'Prazo de pagamento: 140 meses', 'ENTRADA: R$ 16.646,00', 'Valor das demais parcelas: R$ 4.646,00', 'Lance que vai ser descontado do crédito: R$ 168.000,00', 'Crédito que sobra descontado o lance: R$ 312.000,00', 'Saldo após contemplação por lance: 139 parcelas FIXAS de R$ 3.437,36', ' ', 1),
(263, 'PESADO 420', 'Taxa de 0,18% ao mês, 2,14% ao ano / Recolocação grupo já em andamento', 139, 'VALOR', '420.000,00', 'PRAZO DE PAGAMENTO: 140 meses', 'ENTRADA: R$ 14.566,00', 'Valor das demais parcelas: R$ 4.066,00', 'Lance que vai ser descontado do crédito: R$ 147.000,00', 'Crédito que sobra descontado o lance: R$ 273.000,00', 'Saldo após contemplação por lance: 139 parcelas FIXAS de R$ 3.009,44', ' ', 1),
(264, 'PESADO 540', 'Taxa de 0,18% ao mês, 2,14% ao ano / Recolocação grupo já em andamento', 141, 'VALOR', '540.000,00', 'Prazo de pagamento: 140 meses', 'ENTRADA: R$ 18.726,00', 'Valor das demais parcelas: R$ 5.226,00', 'Lance que vai ser descontado do crédito: R$ 189.000,00', 'Crédito que sobra descontado o lance: R$ 351.000,00', 'Saldo após contemplação por lance: 139 parcelas FIXAS de R$ 3.866,28', ' ', 1),
(266, 'CAIXA 1000', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 86, 'CAIXA', '1.000.000,00', 'Prazo de pagamento: 197 meses', 'Valor das parcelas: R$ 6.345,18', 'Lance que vai ser descontado do crédito: R$ 500.000,00', 'Crédito que sobra descontado o lance: R$ 500.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 3.891,28', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(267, 'CAIXA 500', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 83, 'CAIXA', '500.000,00', 'Prazo de pagamento: 197 Meses', 'Valor das parcelas: R$ 3.172,59', 'Lance que vai ser descontado do crédito: R$ 250.000,00', 'Crédito que sobra descontado o lance: R$ 250.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 1.945,64', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(268, 'CAIXA 4000', 'Taxa de 0,09% ao mês, 1,08% ao ano / Tem 5% de fundo de reserva ', 100, 'CAIXA', '4.000.000,00', 'Prazo de pagamento: 196 meses', 'Valor das parcelas: R$ 25.102,04', 'Lance que vai ser descontado do crédito: R$ 2.000.000,00', 'Crédito que sobra descontado o lance: R$ 2.000.000,00', 'Saldo após contemplação por lance: 195 parcelas de R$ 14.845,63', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(272, 'CAIXA 800', 'Taxa de 0,10% ao mês, 1,20% ao ano / Tem 5% de fundo de reserva.', 85, 'CAIXA', '800.000,00', 'Prazo de pagamento: 197 Meses', 'Entrada: R$ 5.076,14', 'Lance que vai ser descontado do crédito: R$ 400.000,00', 'Crédito que sobra descontado o lance: R$ 400.000,00', 'Saldo após contemplação por lance: 196 parcelas de R$ 3.035,32', 'Saldo após contemplação por lance: R$ ', ' ', 2),
(273, 'VALOR  3.30% 800', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 5, 'VALOR', '800.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'Entrada: R$ 31.388,00', 'Valor das demais parcelas: R$ 4.988,00', 'Lance que vai ser descontado do crédito: R$ 280.000,00', 'Crédito que sobra descontado o lance: R$ 520.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 3.581,08', ' ', 1),
(274, 'VALOR 400', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 1, 'VALOR', '400.000,00', 'Prazo de pagamento: 200 meses', 'Valor da Entrada: R$ 28.164,00', 'PAGA A ENTRADA E TEM 6 MESES DE CARÊNCIA ', 'Lance que vai ser descontado do crédito: R$ 80.000,00', 'Crédito que sobra descontado o lance: R$ 320.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 2.009,83', ' ', 1),
(275, 'VALOR 3,30% 9000', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 12, 'VALOR', '9.000.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'Entrada: R$ 353.115,00', 'Valor das demais parcelas: R$ 56.115,00', 'Lance que vai ser descontado do crédito: R$ 1.800.000,00', 'Crédito que sobra descontado o lance: R$ 7.200.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 47.069,77', ' ', 1),
(278, 'VALOR 600', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 2, 'VALOR', '600.000,00', 'Prazo de pagamento: 200 meses', 'Valor da Entrada: R$ 42.246,00', 'PAGA A ENTRADA E TEM 6 MESES DE CARÊNCIA ', 'Lance que vai ser descontado do crédito: R$ 120.000,00', 'Crédito que sobra descontado o lance: R$ 480.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 3.039,05', ' ', 1),
(279, 'VALOR 800', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 3, 'VALOR', '800.000,00', 'Prazo de pagamento: 200 meses', 'Valor da Entrada: R$ 56.328,00', 'PAGA A ENTRADA E TEM 6 MESES DE CARÊNCIA ', 'Lance que vai ser descontado do crédito: R$ 160.000,00', 'Crédito que sobra descontado o lance: R$ 620.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 4.019,66', ' ', 1),
(280, 'VALOR 1,0M', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 4, 'VALOR', '1.000.000,00', 'Prazo de pagamento: 200 meses', 'Valor da Entrada: R$ 70.410,00', 'PAGA A ENTRADA E TEM 6 MESES DE CARÊNCIA ', 'Lance que vai ser descontado do crédito: R$ 200.000,00', 'Crédito que sobra descontado o lance: R$ 800.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 30.390,46', ' ', 1),
(281, 'VALOR 2,5M', 'Taxa de 0,14% ao mês, 1,68% ao ano / Recolocação grupo já em andamento', 4, 'VALOR', '2.500.000,00', 'PRAZO DE PAGAMENTO: 200 meses', 'Valor da Entrada: R$ 176.025,00', 'PAGA A ENTRADA E TEM 6 MESES DE CARÊNCIA ', 'Lance que vai ser descontado do crédito: R$ 500.000,00', 'Crédito que sobra descontado o lance: R$ 2.000.000,00', 'Saldo após contemplação por lance: 199 parcelas FIXAS de R$ 12.604,96', ' ', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `grupo` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `login`, `password`, `grupo`, `email`, `senha`) VALUES
(1, 'RAFAEL', 'admin', '3791645j', 2, 'rafael@parceriaaf.com.br', '3791645j'),
(2, 'Vendedor 1', 'teste1', 'teste', 1, 'teste@teste.com', ''),
(3, 'Vendedor 2', 'teste2', 'teste', 1, 'teste@teste.com', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `historico`
--
ALTER TABLE `historico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lembretes`
--
ALTER TABLE `lembretes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `propostas`
--
ALTER TABLE `propostas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `historico`
--
ALTER TABLE `historico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `lembretes`
--
ALTER TABLE `lembretes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `propostas`
--
ALTER TABLE `propostas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=282;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
